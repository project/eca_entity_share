<?php

namespace Drupal\eca_entity_share_server\Plugin\ECA\Event;

use Drupal\eca\Plugin\ECA\Event\EventDeriverBase;

/**
 * Deriver for ECA Entity Share Server event plugins.
 */
class EntityShareServerEventDeriver extends EventDeriverBase {

  /**
   * {@inheritdoc}
   */
  protected function definitions(): array {
    return EntityShareServerEvent::definitions();
  }

}
