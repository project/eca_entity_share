<?php

namespace Drupal\eca_entity_share_server\Plugin\ECA\Event;

use Drupal\eca\Plugin\ECA\Event\EventBase;
use Drupal\entity_share_server\Event\ChannelListEvent;

/**
 * Plugin implementation of the ECA Events for entity share server.
 *
 * @EcaEvent(
 *   id = "entity_share_server",
 *   deriver = "Drupal\eca_entity_share_server\Plugin\ECA\Event\EntityShareServerEventDeriver",
 *   eca_version_introduced = "1.0.0"
 * )
 */
class EntityShareServerEvent extends EventBase {

  /**
   * {@inheritdoc}
   */
  public static function definitions(): array {
    return [
      'rel_field_value' => [
        'label' => 'Entity share: Channel list prepared',
        'event_name' => ChannelListEvent::EVENT_NAME,
        'event_class' => ChannelListEvent::class,
      ],
    ];
  }

}
