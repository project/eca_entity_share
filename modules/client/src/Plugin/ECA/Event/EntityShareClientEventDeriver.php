<?php

namespace Drupal\eca_entity_share_client\Plugin\ECA\Event;

use Drupal\eca\Plugin\ECA\Event\EventDeriverBase;

/**
 * Deriver for ECA Entity Share Client event plugins.
 */
class EntityShareClientEventDeriver extends EventDeriverBase {

  /**
   * {@inheritdoc}
   */
  protected function definitions(): array {
    return EntityShareClientEvent::definitions();
  }

}
