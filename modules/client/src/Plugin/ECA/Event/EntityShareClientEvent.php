<?php

namespace Drupal\eca_entity_share_client\Plugin\ECA\Event;

use Drupal\eca\Plugin\ECA\Event\EventBase;
use Drupal\entity_share_client\Event\RelationshipFieldValueEvent;

/**
 * Plugin implementation of the ECA Events for entity share client.
 *
 * @EcaEvent(
 *   id = "entity_share_client",
 *   deriver = "Drupal\eca_entity_share_client\Plugin\ECA\Event\EntityShareClientEventDeriver",
 *   eca_version_introduced = "1.0.0"
 * )
 */
class EntityShareClientEvent extends EventBase {

  /**
   * {@inheritdoc}
   */
  public static function definitions(): array {
    return [
      'rel_field_value' => [
        'label' => 'Entity share: Relationship Field Value',
        'event_name' => RelationshipFieldValueEvent::EVENT_NAME,
        'event_class' => RelationshipFieldValueEvent::class,
      ],
    ];
  }

}
